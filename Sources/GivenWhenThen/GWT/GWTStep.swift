//
//  GWTStep.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 14.10.20.
//

import Foundation

/// Possible steps to be used with `GWT` values.
public enum GWTStep: String {
	/// A FEATURE. Contains one or more Scenarios or Scenario Outlines.
	case feature = "Feature:"
	/// A set of steps to execute before every scenario of a feature.
	case background = "Background:"
	/// A SCENARIO OUTLINE step. Use scenario outlines for running scenarios
	/// with an array of example values.
	/// The crux of a Scenario is defined by a sequence of Steps outlining the
	/// preconditions and flow of events that will take place.
	case scenarioOutline = "Scenario Outline:"
	/// A SCENARIO step. Use scenarios for grouping together chains of steps.
	/// The crux of a Scenario is defined by a sequence of Steps outlining the
	/// preconditions and flow of events that will take place.
	case scenario = "Scenario:"
	/// A GIVEN step. Use as starting point for GWT chains, defines a precondition.
	/// Describes the preconditions and initial state before the start of a test
	/// and allows for any pre-test setup that may occur.
	case given
	/// A WHEN step. Use as a step to define an action.
	/// Describes actions taken by a user during a test.
	case when
	/// A THEN step. Use as a step to define a postcondition.
	/// Describes the outcome resulting from actions taken in the `when` step.
	case then

	/// An AND step. Use as a step to extend the preceding step using logical `&&`.
	case and
	/// A BUT step. Logically the same as `and`, but used in the negative form.
	case but

	/// Prefixes the step name to the passed name.
	/// - Parameter name: The name of the step.
	/// - Returns: `name` prefixed with the name of the step, uppercased.
	public func callAsFunction(_ name: String) -> String {
		"\(rawValue.uppercased()) \(name)"
	}
}
