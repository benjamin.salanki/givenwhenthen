//
//  Steps.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 29.11.20.
//

import Foundation

public protocol Steps {
	var steps: [Step] { get }
}
