//
//  FeatureBuilder.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 26.10.20.
//

import Foundation

@_functionBuilder
public struct FeatureBuilder {
	public static func buildBlock(_ scenarios: [Scenario]...) -> [Scenario] { scenarios.flatMap { $0 } }
	public static func buildBlock(_ background: Background, _ scenarios: [Scenario]...) -> (Background?, [Scenario]) { (background, scenarios.flatMap { $0 }) }
}
