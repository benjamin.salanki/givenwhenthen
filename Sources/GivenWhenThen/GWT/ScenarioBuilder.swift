//
//  ScenarioBuilder.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 26.10.20.
//

import Foundation

@_functionBuilder
public struct ScenarioBuilder {
	public static func buildBlock(_ steps: Steps) -> Steps { steps }
}
