//
//  Step.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 29.11.20.
//

import Foundation

public struct Step {
	public let name: String
	public let block: (Any?) throws -> Any?
}
