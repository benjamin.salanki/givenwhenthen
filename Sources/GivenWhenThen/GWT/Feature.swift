//
//  Feature.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 29.11.20.
//

import Foundation

public struct Feature {
	public let scenarios: [Scenario]
	public let background: Steps?

	public init(scenarios: [Scenario]) {
		self.scenarios = scenarios
		self.background = nil
	}

	public init(scenarios: (Steps?, [Scenario])) {
		self.scenarios = scenarios.1
		self.background = scenarios.0
	}
}
