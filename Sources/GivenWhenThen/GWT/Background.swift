//
//  Background.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 29.11.20.
//

import Foundation

public final class Background: Steps {

	public var steps: [Step]

	// MARK: - Initialization

	required public init(_ steps: [Step]) {
		self.steps = steps
	}
}
