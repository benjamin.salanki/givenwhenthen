//
//  Scenario.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 29.11.20.
//

import Foundation

public struct Scenario {
	public let name: String
	public let steps: Steps
}
