//
//  GivenWhenThen.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 14.10.20.
//

import XCTest

/// A class representing a step in a Given-When-Then (GWT) chain.
///
/// Start your chain with any of the `XCTestCase.given(...)` methods, then
/// chain them in the format `GIVEN-(AND|BUT)?-WHEN?-(AND|BUT)?-THEN-(AND|BUT)?`
/// where `?` denotes an optional step and steps in `()` can be mixed and matched
/// multiple times or left out.
///
/// `Output` specifies the step the chain is currently in, based on
/// the [Hoare Triple](https://en.wikipedia.org/wiki/Hoare_logic#Hoare_triple).
///
/// `{GIVEN} WHEN {THEN}`, where `GIVEN` and `THEN` are assertions and `WHEN` is a
/// command.
public class GWTChain<Value, Output>: Steps {

	/// The steps the GWT chain consists of.
	public var steps: [Step]

	// MARK: - Initialization

	required public init(_ steps: [Step]) {
		self.steps = steps
	}

	// MARK: - WHEN

	/// Creates a `GWTStep` of type `when` as an activity and runs it.
	///
	/// This step can only follow the `given` step, or any subsequent `and` or
	/// `but` step, as long as the `Output` of those steps is of type `Precondition`.
	/// Steps following this step can be of the following types: `and, but, then`.
	/// The `Output` of this step is `Command`.
	/// - Parameters:
	///   - description: The description of the `when` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func when<ResultValue, Result: GWTChain<ResultValue, Command>>(_ description: String, block: @escaping (Value) throws -> ResultValue) rethrows -> Result where Output == Precondition {
		steps.append(Step(name: GWTStep.when(description), block: { try block($0 as! Value) }))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `when` as an activity and runs it.
	///
	/// This step can only follow the `given` step, or any subsequent `and` or
	/// `but` step, as long as the `Output` of those steps is of type `Precondition`.
	/// Steps following this step can be of the following types: `and, but, then`.
	/// The `Output` of this step is `Command`.
	/// The `Value` of the previous step is passed through this step to the next step.
	/// - Parameters:
	///   - description: The description of the `when` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value of the receiver.
	@discardableResult
	final public func when<Result: GWTChain<Value, Command>>(_ description: String, block: @escaping (Value) throws -> Void) rethrows -> Result where Output == Precondition {
		steps.append(Step(name: GWTStep.when(description), block: {
			try block($0 as! Value)
			return $0
		}))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `when` as an activity and runs it.
	///
	/// This step can only follow the `given` step, or any subsequent `and` or
	/// `but` step, as long as the `Output` of those steps is of type `Precondition`.
	/// Steps following this step can be of the following types: `and, but, then`.
	/// The `Output` of this step is `Command`.
	/// - Parameter description: The description of the `when` step.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func when<Result: GWTChain<Value, Command>>(_ description: String) -> Result where Output == Precondition {
		steps.append(Step(name: GWTStep.when(description), block: { $0 }))
		return Result(steps)
	}

	// MARK: - THEN

	/// Creates a `GWTStep` of type `then` as an activity and runs it.
	///
	/// This step can only follow the `given` or `when` steps, or any subsequent
	/// `and` or `but` step, as long as the `Output` of those steps is of type
	/// `Command`.
	/// Steps following this step can be of the following types: `and, but`.
	/// The `Output` of this step is `Postcondition`.
	/// - Parameters:
	///   - description: The description of the `then` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func then<ResultValue, Result: GWTChain<ResultValue, Postcondition>>(_ description: String, block: @escaping (Value) throws -> ResultValue) rethrows -> Result where Output == Command {
		steps.append(Step(name: GWTStep.then(description), block: { try block($0 as! Value) }))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `then` as an activity and runs it.
	///
	/// This step can only follow the `given` or `when` steps, or any subsequent
	/// `and` or `but` step, as long as the `Output` of those steps is of type
	/// `Command`.
	/// Steps following this step can be of the following types: `and, but`.
	/// The `Output` of this step is `Postcondition`.
	/// The `Value` of the previous step is passed through this step to the next step.
	/// - Parameters:
	///   - description: The description of the `then` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value of the receiver.
	@discardableResult
	final public func then<Result: GWTChain<Value, Postcondition>>(_ description: String, block: @escaping (Value) throws -> Void) rethrows -> Result where Output == Command {
		steps.append(Step(name: GWTStep.then(description), block: {
			try block($0 as! Value)
			return $0
		}))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `then` as an activity and runs it.
	///
	/// This step can only follow the `given` or `when` steps, or any subsequent
	/// `and` or `but` step, as long as the `Output` of those steps is of type
	/// `Command`.
	/// Steps following this step can be of the following types: `and, but`.
	/// The `Output` of this step is `Postcondition`.
	/// - Parameter description: The description of the `then` step.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func then<Result: GWTChain<Value, Postcondition>>(_ description: String) -> Result where Output == Command {
		steps.append(Step(name: GWTStep.then(description), block: { $0 }))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `then` as an activity and runs it.
	///
	/// This step can only follow the `given` or `when` steps, or any subsequent
	/// `and` or `but` step, as long as the `Output` of those steps is of type
	/// `Command`.
	/// Steps following this step can be of the following types: `and, but`.
	/// The `Output` of this step is `Postcondition`.
	/// - Parameters:
	///   - description: The description of the `then` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func then<ResultValue, Result: GWTChain<ResultValue, Postcondition>>(_ description: String, block: @escaping (Value) throws -> ResultValue) rethrows -> Result where Output == Precondition {
		steps.append(Step(name: GWTStep.then(description), block: { try block($0 as! Value) }))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `then` as an activity and runs it.
	///
	/// This step can only follow the `given` or `when` steps, or any subsequent
	/// `and` or `but` step, as long as the `Output` of those steps is of type
	/// `Command`.
	/// Steps following this step can be of the following types: `and, but`.
	/// The `Output` of this step is `Postcondition`.
	/// The `Value` of the previous step is passed through this step to the next step.
	/// - Parameters:
	///   - description: The description of the `then` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value of the receiver.
	@discardableResult
	final public func then<Result: GWTChain<Value, Postcondition>>(_ description: String, block: @escaping (Value) throws -> Void) rethrows -> Result where Output == Precondition {
		steps.append(Step(name: GWTStep.then(description), block: {
			try block($0 as! Value)
			return $0
		}))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `then` as an activity and runs it.
	///
	/// This step can only follow the `given` or `when` steps, or any subsequent
	/// `and` or `but` step, as long as the `Output` of those steps is of type
	/// `Command`.
	/// Steps following this step can be of the following types: `and, but`.
	/// The `Output` of this step is `Postcondition`.
	/// - Parameter description: The description of the `then` step.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func then<Result: GWTChain<Value, Postcondition>>(_ description: String) -> Result where Output == Precondition {
		steps.append(Step(name: GWTStep.then(description), block: { $0 }))
		return Result(steps)
	}

	// MARK: - AND

	/// Creates a `GWTStep` of type `and` as an activity and runs it.
	///
	/// This step can follow any step.
	/// Steps following this step depend on the `Output` of the previous step.
	/// The `Output` of this step is the same as that of step preceding this.
	/// - Parameters:
	///   - description: The description of the `and` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func and<ResultValue, Result: GWTChain<ResultValue, Output>>(_ description: String, block: @escaping (Value) throws -> ResultValue) rethrows -> Result {
		steps.append(Step(name: GWTStep.and(description), block: { try block($0 as! Value) }))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `and` as an activity and runs it.
	///
	/// This step can follow any step.
	/// Steps following this step depend on the `Output` of the previous step.
	/// The `Output` of this step is the same as that of step preceding this.
	/// The `Value` of the previous step is passed through this step to the next step.
	/// - Parameters:
	///   - description: The description of the `and` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value of the receiver.
	@discardableResult
	final public func and<Result: GWTChain<Value, Output>>(_ description: String, block: @escaping (Value) throws -> Void) rethrows -> Result {
		steps.append(Step(name: GWTStep.and(description), block: {
			try block($0 as! Value)
			return $0
		}))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `and` as an activity and runs it.
	///
	/// This step can follow any step.
	/// Steps following this step depend on the `Output` of the previous step.
	/// The `Output` of this step is the same as that of step preceding this.
	/// - Parameter description: The description of the `and` step.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func and<Result: GWTChain<Value, Output>>(_ description: String) -> Result {
		steps.append(Step(name: GWTStep.and(description), block: { $0 }))
		return Result(steps)
	}

	// MARK: - BUT

	/// Creates a `GWTStep` of type `but` as an activity and runs it.
	///
	/// This step can follow any step.
	/// Steps following this step depend on the `Output` of the previous step.
	/// The `Output` of this step is the same as that of step preceding this.
	/// - Parameters:
	///   - description: The description of the `but` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func but<ResultValue, Result: GWTChain<ResultValue, Output>>(_ description: String, block: @escaping (Value) throws -> ResultValue) rethrows -> Result {
		steps.append(Step(name: GWTStep.but(description), block: { try block($0 as! Value) }))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `but` as an activity and runs it.
	///
	/// This step can follow any step.
	/// Steps following this step depend on the `Output` of the previous step.
	/// The `Output` of this step is the same as that of step preceding this.
	/// The `Value` of the previous step is passed through this step to the next step.
	/// - Parameters:
	///   - description: The description of the `but` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value of the receiver.
	@discardableResult
	final public func but<Result: GWTChain<Value, Output>>(_ description: String, block: @escaping (Value) throws -> Void) rethrows -> Result {
		steps.append(Step(name: GWTStep.but(description), block: {
			try block($0 as! Value)
			return $0
		}))
		return Result(steps)
	}

	/// Creates a `GWTStep` of type `but` as an activity and runs it.
	///
	/// This step can follow any step.
	/// Steps following this step depend on the `Output` of the previous step.
	/// The `Output` of this step is the same as that of step preceding this.
	/// - Parameter description: The description of the `but` step.
	/// - Returns: The value returned by `block`.
	@discardableResult
	final public func but<Result: GWTChain<Value, Output>>(_ description: String) -> Result {
		steps.append(Step(name: GWTStep.but(description), block: { $0 }))
		return Result(steps)
	}
}

// MARK: - Hoare Triple

/// Protocol defining a [Hoare Precondition](https://en.wikipedia.org/wiki/Hoare_logic#Hoare_triple)
public protocol Precondition {}
/// Protocol defining a [Hoare Command](https://en.wikipedia.org/wiki/Hoare_logic#Hoare_triple)
public protocol Command {}
/// Protocol defining a [Hoare Postcondition](https://en.wikipedia.org/wiki/Hoare_logic#Hoare_triple)
public protocol Postcondition {}
