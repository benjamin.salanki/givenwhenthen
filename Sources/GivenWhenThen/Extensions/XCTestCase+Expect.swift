//
//  XCTestCase+Expect.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 14.10.20.
//

import XCTest

public extension XCTestCase {

	// MARK: - Boolean

	/// Asserts that an expression is `true`.
	///
	/// This method generates a failure when `expression == false`.
	/// - Parameters:
	///   - expression: An expression of type `Bool`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression` parameter.
	@discardableResult
	func expect(isTrue expression: @autoclosure () throws -> Bool, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> Bool {
		let expression = try expression()
		let message = message()
		XCTAssertTrue(expression, message, file: file, line: line)
		return expression
	}

	/// Asserts that an expression is `false`.
	///
	/// This method generates a failure when `expression == true`.
	/// - Parameters:
	///   - expression: An expression of type `Bool`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression` parameter.
	@discardableResult
	func expect(isFalse expression: @autoclosure () throws -> Bool, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> Bool {
		let expression = try expression()
		let message = message()
		XCTAssertFalse(expression, message, file: file, line: line)
		return expression
	}

	// MARK: - Optionals

	/// Asserts that an expression is `nil`.
	///
	/// This method generates a failure when `expression != nil`.
	/// - Parameters:
	///   - expression: An expression of type `Any?` to compare against `nil`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression` parameter.
	@discardableResult
	func expect<T>(isNil expression: @autoclosure () throws -> T?, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> T? {
		let expression = try expression()
		let message = message()
		XCTAssertNil(expression, message, file: file, line: line)
		return expression
	}

	/// Asserts that an expression is not `nil`.
	///
	/// This method generates a failure when `expression == nil`.
	/// - Parameters:
	///   - expression: An expression of type `Any?` to compare against `nil`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression` parameter.
	@discardableResult
	func expect<T>(isNotNil expression: @autoclosure () throws -> T?, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> T? {
		let expression = try expression()
		let message = message()
		XCTAssertNotNil(expression, message, file: file, line: line)
		return expression
	}

	// MARK: - Less Than

	/// Asserts that the value of the first expression is less than the value of
	/// the second expression.
	/// - Parameters:
	///   - expression1: An expression of type `T`, where `T` is `Comparable`.
	///   - expression2: An expression of type `T`, where `T` is `Comparable`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression1` and `expression2` parameters,
	/// expressed as a tuple of `(T, T)`.
	@discardableResult
	func expect<T>(_ expression1: @autoclosure () throws -> T, isLessThan expression2: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> (T, T) where T : Comparable {
		let expression1 = try expression1()
		let expression2 = try expression2()
		let message = message()
		XCTAssertLessThan(expression1, expression2, message, file: file, line: line)
		return (expression1, expression2)
	}

	/// Asserts that the value of the first expression is less than or equal the
	/// value of the second expression.
	/// - Parameters:
	///   - expression1: An expression of type `T`, where `T` is `Comparable`.
	///   - expression2: An expression of type `T`, where `T` is `Comparable`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression1` and `expression2` parameters,
	/// expressed as a tuple of `(T, T)`.
	@discardableResult
	func expect<T>(_ expression1: @autoclosure () throws -> T, isLessThanOrEqual expression2: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> (T, T) where T : Comparable {
		let expression1 = try expression1()
		let expression2 = try expression2()
		let message = message()
		XCTAssertLessThanOrEqual(expression1, expression2, message, file: file, line: line)
		return (expression1, expression2)
	}

	// MARK: - Equality

	/// Asserts that two values are equal.
	///
	///	Use this function to compare two non-optional values of the same type.
	/// - Parameters:
	///   - expression1: An expression of type `T`, where `T` is `Equatable`.
	///   - expression2: An expression of type `T`, where `T` is `Equatable`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression1` and `expression2` parameters,
	/// expressed as a tuple of `(T, T)`.
	@discardableResult
	func expect<T>(_ expression1: @autoclosure () throws -> T, isEqual expression2: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> (T, T) where T : Equatable {
		let expression1 = try expression1()
		let expression2 = try expression2()
		let message = message()
		XCTAssertEqual(expression1, expression2, message, file: file, line: line)
		return (expression1, expression2)
	}

	/// Asserts that two values are not equal.
	/// - Parameters:
	///   - expression1: An expression of type `T`, where `T` is `Equatable`.
	///   - expression2: An expression of type `T`, where `T` is `Equatable`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression1` and `expression2` parameters,
	/// expressed as a tuple of `(T, T)`.
	@discardableResult
	func expect<T>(_ expression1: @autoclosure () throws -> T, isNotEqual expression2: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> (T, T) where T : Equatable {
		let expression1 = try expression1()
		let expression2 = try expression2()
		let message = message()
		XCTAssertNotEqual(expression1, expression2, message, file: file, line: line)
		return (expression1, expression2)
	}

	// MARK: - Greater Than

	/// Asserts that the value of the first expression is greater than or equal
	/// the value of the second expression.
	/// - Parameters:
	///   - expression1: An expression of type `T`, where `T` is `Comparable`.
	///   - expression2: An expression of type `T`, where `T` is `Comparable`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression1` and `expression2` parameters,
	/// expressed as a tuple of `(T, T)`.
	@discardableResult
	func expect<T>(_ expression1: @autoclosure () throws -> T, isGreaterThanOrEqual expression2: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> (T, T) where T : Comparable {
		let expression1 = try expression1()
		let expression2 = try expression2()
		let message = message()
		XCTAssertGreaterThanOrEqual(expression1, expression2, message, file: file, line: line)
		return (expression1, expression2)
	}

	/// Asserts that the value of the first expression is greater than the value
	/// of the second expression.
	/// - Parameters:
	///   - expression1: An expression of type `T`, where `T` is `Comparable`.
	///   - expression2: An expression of type `T`, where `T` is `Comparable`.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Returns: The value passed in the `expression1` and `expression2` parameters,
	/// expressed as a tuple of `(T, T)`.
	@discardableResult
	func expect<T>(_ expression1: @autoclosure () throws -> T, isGreaterThan expression2: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) rethrows -> (T, T) where T : Comparable {
		let expression1 = try expression1()
		let expression2 = try expression2()
		let message = message()
		XCTAssertGreaterThan(expression1, expression2, message, file: file, line: line)
		return (expression1, expression2)
	}

	// MARK: Throwing

	/// Asserts that an expression throws an error.
	/// - Parameters:
	///   - expression: An expression that can throw an error.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	///   - errorHandler: An optional handler for errors that are thrown by `expression`.
	func expect<T>(throws expression: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line, _ errorHandler: (Error) -> Void = { _ in }) rethrows  {
		let message = message()
		XCTAssertThrowsError(try expression(), message, file: file, line: line, errorHandler)
	}

	/// Asserts that an expression doesn’t throw an error.
	/// - Parameters:
	///   - expression: An expression that can throw an error.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	///   - errorHandler: An optional handler for errors that are thrown by `expression`.
	/// - Returns: The result of `expression`.
	@discardableResult
	func expect<T>(noThrow expression: @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line, _ errorHandler: (Error) -> Void = { _ in }) rethrows -> T  {
		let message = message()
		var returnValue: T!
		XCTAssertNoThrow(returnValue = try expression(), message, file: file, line: line)
		return returnValue
	}
}
