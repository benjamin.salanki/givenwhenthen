//
//  XCTestCase+GivenWhenThen.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 12.10.20.
//

import XCTest

public extension XCTestCase {

	// MARK: - Running Activities

	/// Create and run a new activity with provided name and block.
	/// - Parameters:
	///   - name: The name to use for the activity. Displayed in reports.
	///   - block: The block to run for this activity.
	/// - Returns: The return value of the block.
	@discardableResult
	static func runActivity<Result>(_ name: String, block: () throws -> Result) rethrows -> Result {
		try XCTContext.runActivity(named: name, block: { _ in try block() })
	}

	// MARK: - Features

	/// Creates a `GWTStep` of type `feature` as an activity and runs it.
	/// - Parameters:
	///   - description: The description of the feature.
	///   - block: The block that is the body of the resulting activity.
	final func feature(_ description: String, @FeatureBuilder block: () throws -> [Scenario]) throws {
		try runFeature(description, feature: Feature(scenarios: try block()))
	}

	final func feature(_ description: String, @FeatureBuilder block: () throws -> (Steps?, [Scenario])) throws {
		try runFeature(description, feature: Feature(scenarios: try block()))
	}

	private func runFeature(_ description: String, feature: Feature) throws {
		try XCTestCase.runActivity(GWTStep.feature(description)) {

			try feature.scenarios.forEach { scenario in
				try XCTestCase.runActivity(scenario.name) {
					if let background = feature.background {
						try XCTestCase.runActivity(GWTStep.background("")) {
							var result: Any?
							try background.steps.forEach { (step) in
								try XCTestCase.runActivity(step.name) {
									result = try step.block(result)
								}
							}
						}
					}
					var result: Any?
					try scenario.steps.steps.forEach { (step) in
						try XCTestCase.runActivity(step.name) {
							result = try step.block(result)
						}
					}
				}
			}
		}
	}

	// MARK: - Scenarios

	/// Creates a `GWTStep` of type `scenario` as an activity and runs it.
	/// - Parameters:
	///   - description: The description of the scenario.
	///   - block: The block that is the body of the resulting activity.
	final func scenario(_ description: String, @ScenarioBuilder block: () throws -> Steps) rethrows -> [Scenario] {
		[Scenario(name: GWTStep.scenario(description), steps: try block())]
	}

	/// Creates a `GWTStep` of type `scenarioOutline` that supports examples as an activity and runs it.
	/// - Parameters:
	///   - description: The description of the scenario.
	///   - block: The block that is the body of the resulting activity.
	///   - examples: A block returning an array of objects that gets passed sequentially to `block`.
	final func scenarioOutline<T>(_ description: String, @ScenarioBuilder block: (T) throws -> Steps, examples: () throws -> [T]) rethrows -> [Scenario] {
		var scenarios: [Scenario] = []
		try examples().forEach {
			try scenarios.append(Scenario(name: GWTStep.scenario(description), steps: block($0)))
		}
		return scenarios
	}

	// MARK: - Background

	final func background(@ScenarioBuilder block: () throws -> Steps) rethrows -> Background {
		try Background(block().steps)
	}

	// MARK: - Starting a GWT Flow

	/// Creates a `GWTStep` of type `given` as an activity and runs it.
	/// - Parameters:
	///   - description: The description of the `given` step.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	final func given<ResultValue, Result: GWTChain<ResultValue, Precondition>>(_ description: String, block: @escaping (ResultValue?) throws -> ResultValue) rethrows -> Result {
		try given(description, initialValue: nil, block: block)
	}

	/// Creates a `GWTStep` of type `given` as an activity with an initial value
	/// and runs it.
	/// - Parameters:
	///   - description: The description of the `given` step.
	///   - initialValue: The initial value to pass to `block`.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: The value returned by `block`.
	final func given<InitialValue, ResultValue, Result: GWTChain<ResultValue, Precondition>>(_ description: String, initialValue: InitialValue, block: @escaping (InitialValue) throws -> ResultValue) rethrows -> Result {
		Result([Step(name: GWTStep.given(description), block: { _ in try block(initialValue) })])
	}

	/// Creates a `GWTStep` of type `given` as an activity with an initial value
	/// and runs it.
	/// The `initialValue` is passed through this step to the next step.
	/// - Parameters:
	///   - description: The description of the `given` step.
	///   - initialValue: The initial value to pass to `block`.
	///   - block: The block that is the body of the resulting activity.
	/// - Returns: `initialValue`.
	final func given<InitialValue, Result: GWTChain<InitialValue, Precondition>>(_ description: String, initialValue: InitialValue, block: @escaping (InitialValue) throws -> Void) rethrows -> Result {
		Result([Step(name: GWTStep.given(description), block: { _ in
			try block(initialValue)
			return initialValue
		})])
	}

	/// Creates a `GWTStep` of type `given` as an activity and runs it.
	/// - Parameter description: The description of the `given` step.
	/// - Returns: A `GWT` object with its `Output` set to `Precondition` and
	/// `Value` set to `nil`.
	final func given<Result: GWTChain<String?, Precondition>>(_ description: String) -> Result {
		given(description, initialValue: nil)
	}

	/// Creates a `GWTStep` of type `given` with an initial value as an activity and runs it.
	/// - Parameters:
	///   - description: The description of the `given` step.
	///   - initialValue: The initial value to set as the `Value` of the return value.
	/// - Returns: A `GWT` object with its `Output` set to `Precondition` and
	/// `Value` set to `initialValue`.
	final func given<ResultValue, Result: GWTChain<ResultValue, Precondition>>(_ description: String, initialValue: ResultValue) -> Result {
		Result([Step(name: GWTStep.given(description), block: { _ in
			if let initialValue = initialValue as? ()->() {
				initialValue()
			}
			return initialValue
		})])
	}
}
