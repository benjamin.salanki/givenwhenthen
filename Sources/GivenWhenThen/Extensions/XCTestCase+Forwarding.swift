//
//  XCTestCase+Forwarding.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 14.10.20.
//

import XCTest

public extension XCTestCase {

	/// A method that returns a passed value after the passed block completed processing.
	/// - Parameters:
	///   - result: A value that will be returned by the method.
	///   - block: A block that is triggered before the method returns.
	/// - Returns: The value passed in the `result` parameter.
	@discardableResult
	func forwarding<T>(_ result: @autoclosure () throws -> T, block: () throws -> Void) rethrows -> T {
		try block()
		return try result()
	}
}
