# GivenWhenThen

Behavior driven tests for Xcode.

## Requirements

Minimum Swift Tools version: 5.3.
Minimum iOS version: 13.

## GivenWhenThen

GivenWhenThen enables writing behavior driven tests in Xcode easy and straightforward.
Built on the reference outlined by [Gherkin](https://cucumber.io/docs/gherkin/reference/),
`GivenWhenThen` supports the following Gherkin concepts:

1. Features
2. Scenarios
3. Steps (`given`, `when`, `then`, `and`, `but`)
4. Backgrounds
5. Scenario Outlines with Examples

### Features

The purpose of a `feature` is to provide a high-level description of a software feature, and to 
group related `scenario`s. When writing tests using `GivenWhenThen`, all scenarios _must_ be 
enclosed in a `feature` or their steps will not be interpreted during a test run.

```swift
/// A test using `GivenWhenThen`.
func testUsingGivenWhenThen() throws {
	try feature("testGivenWhenThen") {
		// Add optional Background and at least one Scenario or Scenario Outline.
	}
}
```

Features are throwing methods, and they need to be placed in a regular `XCTestCase` test 
throwing function to work, as shown in the above example. As best practice, try adding a
single `feature` per test `func`.

### Scenarios

A `scenario` is a concrete example that illustrates a business rule. It consists of a list of `steps`.
You can have as many `steps` as you like, but as best practice it is recommend to keep the 
number at 3-5 per `scenario`. Having too many `steps` in a `scenario` will cause it to lose it’s 
expressive power as specification and documentation.

In addition to being a specification and documentation, a `scenario` is also a test. As a whole, 
your `scenarios` are an executable specification of the system.

`Scenarios` follow this same pattern:

Describe an initial context (`given` steps)
Describe an event (`when` steps)
Describe an expected outcome (`then` steps)

```swift
let startingAmount = Int.random(in: 5...10)
let spendingAmount = Int.random(in: 1...4)
scenario("Given When Then") {
	given("Ben has some bitcoins") { _ in }
	.when("he spends \(spendingAmount)")
	.then("he has less bitcoins than when he started")
	.but("he still has bitcoins to spend")
	.and("this is true when inverted as well")
}
```

The above example shows how `steps` could be chained inside a `scenario`. While this 
particular example does not do any business logic, it describes the behavior that the test is 
expected to validate. This documentation will build on this example step by step to explain
how to incorporate business logic and build powerful, behavior driven tests with `GivenWhenThen`.

### Steps

Each `step` starts with `given`, `when`, `then`, `and`, or `but`.

Each `step` in a `scenario` is executed one at a time, in the sequence they are added to the 
scenario. To ensure that a certain `step` can only be performed after another `step` has already
been performed, `GivenWhenThen` offers the ability to chain these `steps`. This means that
a `scenario` must start its `steps` with `given`, which can then be followed by steps in the
following order:

 `given (and*, but*)? (when)? (and*, but*)? (then)? (and*, but*)?`

You are limited to one set of `steps` per `scenario`. This is enforced by the compiler.

#### Given

`Given` steps are used to describe the initial context of the system - the scene of the `scenario`. 
It is typically something that happened in the past.

The purpose of `given` steps is to put the system in a known state before the user (or external 
system) starts interacting with the system (in the `when` steps). Avoid talking about user interaction 
in `given`.

>**Examples**
>Mickey and Minnie have started a game
>I am logged in
>Joe has a balance of £42


It’s okay to have several steps to define the initial state. Use `given` in combination with `and` 
and/or `but` to extend the functionality of `given`.

```swift
scenario("Given When Then") {
	given("a starting amount") { _ in
		startingAmount = Int.random(in: 5...10)
	}
	.and("a spending amount") {
		spendingAmount = Int.random(in: 1...4)
	}
}
```

#### When

`When` steps are used to describe an event, or an action. This can be a person interacting with 
the system, or it can be an event triggered by another system.

You can have at most one `when` step per `scenario`. If you feel compelled to add more, it’s 
usually a sign that you should split the scenario up into multiple scenarios.

>**Examples**
>Guess a word
>Invite a friend
>Withdraw money

```swift
let startingAmount = Int.random(in: 5...10)
let spendingAmount = Int.random(in: 1...4)
scenario("Given When Then") {
	given("Ben has some bitcoins") { _ in
		startingAmount
	}
	.when("he spends \(spendingAmount)") { startingAmount in
		(startingAmount, startingAmount - spendingAmount)
	}
	.then("he has less bitcoins than when he started")
	.but("he still has bitcoins to spend")
	.and("this is true when inverted as well")
}

```

As the above example shows, a `step` can pass values down the chain, with each `step` receiving 
the return value of the previous `step` as an input parameter of its block. If a block does not 
provide a return value, the input parameter of the block is passed on to the next `step`. 

The above `given` step passes down `startingAmount`, which is picked up by the `when`
step to calculate the actual amount spent. This `step` modifies the data passed down the chain
by implicitly returning a `tuple` of type `(Int, Int)`, with the `startingAmount` in position
`0` and the remaining amount in position `1`.

Optionally, you can specify an initial value to pass down the chain when writing your `given`
step, which would then be passed in the `step`'s block as an input parameter.

#### Then

`Then` steps are used to describe an expected outcome, or result. 

The block of a `then` step should use an assertion to compare the actual outcome (what the 
system actually does) to the expected outcome (what the `step` says the system is supposed to do).

As best practice, try to use the different `expect()` methods defined in `XCTestCase+Expect.swift` 
that wrap the various `XCTAssert` functions offering a more compact way to assert that can be
chained as well.

>**Examples**
>See that the guessed word was wrong
>Receive an invitation
>Card should be swallowed

```swift
let startingAmount = Int.random(in: 5...10)
let spendingAmount = Int.random(in: 1...4)
scenario("Given When Then") {
	given("Ben has some bitcoins") { _ in
		startingAmount
	}
	.when("he spends \(spendingAmount)") { startingAmount in
		(startingAmount, startingAmount - spendingAmount)
	}
	.then("he has less bitcoins than when he started") { (startingAmount, remainingAmount) in
		expect(remainingAmount, isLessThan: startingAmount)
	}
	.but("he still has bitcoins to spend")
	.and("this is true when inverted as well")
}
```

The above example expands on the previous example and uses `expect(_:isLessThan:)` to
assert the expected outcome. Notice also how the returned `tuple` from the `when` step is now
the input parameter of the block for the `then` step.

#### And, But

`And` and `but` offer the ability to extend the functionality of the steps preceding them.  

So instead of writing the test cases like this:

>Scenario: Multiple Givens
>    Given one thing
>    Given another thing
>    Given yet another thing
>    When I open my eyes
>    Then I should see something
>    Then I shouldn't see something else

Use `and` and `but` to write it like this:

>Scenario: Multiple Givens
>    Given one thing
>    And another thing
>    And yet another thing
>    When I open my eyes
>    Then I should see something
>    But I shouldn't see something else

```swift
let startingAmount = Int.random(in: 5...10)
let spendingAmount = Int.random(in: 1...4)
scenario("Given When Then") {
	given("Ben has some bitcoins") { _ in
		startingAmount
	}
	.when("he spends \(spendingAmount)") { startingAmount in
		(startingAmount, startingAmount - spendingAmount)
	}
	.then("he has less bitcoins than when he started") { (startingAmount, remainingAmount) in
		expect(remainingAmount, isLessThan: startingAmount)
	}
	.but("he still has bitcoins to spend") { (remainingAmount, _) in
		expect(remainingAmount, isGreaterThan: 0)
	}
	.and("this is true when inverted as well") { (remainingAmount, zero) in
		expect(zero, isLessThan: remainingAmount)
	}
}
```

Notice how the `but` step has `remainingAmount` in position `0` of the `tuple`, while the 
preceding `then` step has it at position `1`. This is because `steps` implicitly return single line
results if they have a return value, and `expect(_:isLessThan:)`, as well as all `expect()` 
methods return the values they are passed as a `tuple` in the order they receive them. 
So `expect(remainingAmount, isLessThan: startingAmount)` returns
`(remainingAmount, startingAmount)`.

If this is not what you want, wrap your block using `forwarding(_:completion:)` and pass
in the original value:

```swift
.then("he has less bitcoins than when he started") { (startingAmount, remainingAmount) in
	forwarding((startingAmount, remainingAmount)) {
		expect(remainingAmount, isLessThan: startingAmount)
	}
}
```

You can use `forwarding(_:completion:)` to pass on any value you like down the chain.
In the above example the `but` step following the `then` step does not actually require a `tuple`,
it only needs `remainingAmount`. Knowing this, you could rewrite the `then` and `but` steps like
this:

```swift
.then("he has less bitcoins than when he started") { (startingAmount, remainingAmount) in
	forwarding(remainingAmount) {
		expect(remainingAmount, isLessThan: startingAmount)
	}
}
.but("he still has bitcoins to spend") { remainingAmount in
	expect(remainingAmount, isGreaterThan: 0)
}
```

### Background

Occasionally you’ll find yourself repeating the same `given` steps in all of the `scenarios` in a 
`feature`.

Since it is repeated in every `scenario`, this is an indication that those steps are not essential 
to describe the `scenarios`, they are incidental details. You can move such steps to the 
background, by grouping them under a `background` section.

A `background` allows you to add some context to the `scenarios` that follow it. It can contain
a `given` step chain, which is run before each `scenario`.

A `background` is placed before the first `scenario` in a `feature`. You can have at most one
background, which the compiler ensures. If you need different `background` steps for different 
`scenarios`, consider breaking up your set of `scenarios` into more `features`.

Using all the additional changes described in the preceding sections, the example we built so
far can be extended with a `background` like so:

```swift
var startingAmount = 0
var spendingAmount = 0
try feature("testGivenWhenThen") { [self] in
	background {
		given("random values for starting and spending amount") {
			startingAmount = Int.random(in: 5...10)
			spendingAmount = Int.random(in: 1...4)
		}
	}
	scenario("Given When Then (with blocks)") {
		given("Ben has some bitcoins") { _ in
			startingAmount
		}
		.when("he spends \(spendingAmount)") { startingAmount in
			(startingAmount, startingAmount - spendingAmount)
		}
		.then("he has less bitcoins than when he started") { (startingAmount, remainingAmount) in
			forwarding(remainingAmount) {
				expect(remainingAmount, isLessThan: startingAmount)
			}
		}
		.but("he still has bitcoins to spend") { remainingAmount in
			expect(remainingAmount, isGreaterThan: 0)
		}
		.and("this is true when inverted as well") { (remainingAmount, zero) in
			expect(zero, isLessThan: remainingAmount)
		}
	}
}
```

While the above example has only a single `scenario`, it could be easily extended to support 
more, and all of the subsequent `scenarios` would have a new set of random `startingAmount` 
and `spendingAmount` values thanks to the `background` section that is run before every
`scenario`.

### Scenario Outline

A `scenarioOutline` can be used to run the same `scenario` multiple times, with different 
combinations of values.

Copying and pasting scenarios to use different values quickly becomes tedious and repetitive:

>Scenario: eat 5 out of 12
>  Given there are 12 cucumbers
>  When I eat 5 cucumbers
>  Then I should have 7 cucumbers
>
>Scenario: eat 5 out of 20
>  Given there are 20 cucumbers
>  When I eat 5 cucumbers
>  Then I should have 15 cucumbers

We can collapse these two similar scenarios into a `scenarioOutline`.

A `scenarioOutline` must specify an `examples` array that contains the values that the 
`scenario` receives one at a time. The `scenarioOutline` is run once for each item in the 
`examples` array.

```swift
scenarioOutline("Scenario Outline with Examples") { example in
	given("A list of examples")
		.then("\(example) is one of them") { _ in
			expect(example, isGreaterThanOrEqual: 0)
		}
}
examples: {
	(0..<Int.random(in: 1...5)).map { $0 }
}
```

The above code demonstrates how a `scenarioOutline` receives a random number of `Int`
values from its `examples` block. The `given` step chain is run as many times as there are elements
returned by the `examples` block, and the `example` input parameter of the `scenarioOutline`
block contains a single value from the `examples` array for each iteration.

A `scenarioOutline` can be used everywhere a `scenario` can be used, they are treated as
equals by the compiler.
