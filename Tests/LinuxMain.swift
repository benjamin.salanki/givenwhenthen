import XCTest

import GivenWhenThenTests

var tests = [XCTestCaseEntry]()
tests += GivenWhenThenTests.allTests()
XCTMain(tests)
