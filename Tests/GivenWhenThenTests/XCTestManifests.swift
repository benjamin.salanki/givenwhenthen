import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(XCTestCaseExpectTests.allTests),
		testCase(XCTestCaseGivenWhenThenTests.allTests)
    ]
}
#endif
