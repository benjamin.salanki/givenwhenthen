import XCTest
@testable import GivenWhenThen

final class XCTestCaseGivenWhenThenTests: XCTestCase {

	/// Tests whether `GivenWhenThen` works as expected.
	func testGivenWhenThen() throws {
		var startingAmount = 0
		var spendingAmount = 0
		try feature("testGivenWhenThen") { [self] in
			background {
				given("random values for starting and spending amount") {
					startingAmount = Int.random(in: 5...10)
					spendingAmount = Int.random(in: 1...4)
				}
			}
			scenario("Given When Then (with blocks)") {
				given("Ben has some bitcoins") { _ in
					startingAmount
				}
				.when("he spends \(spendingAmount)") { startingAmount in
					(startingAmount, startingAmount - spendingAmount)
				}
				.then("he has less bitcoins than when he started") { (startingAmount, remainingAmount) in
					expect(remainingAmount, isLessThan: startingAmount)
				}
				.but("he still has bitcoins to spend") { (remainingAmount, _) in
					expect(remainingAmount, isGreaterThan: 0)
				}
				.and("this is true when inverted as well") { (remainingAmount, zero) in
					expect(zero, isLessThan: remainingAmount)
				}
			}

			scenario("Given When Then (no blocks)") {
				given("Ben has some bitcoins", initialValue: startingAmount)
				.when("he sleeps")
				.and("does not spend his bitcoins")
				.then("his wealth does not change")
				.but("he is not awake during this time")
			}

			scenario("Given Then (no when, no blocks)") {
				given("Ben has some bitcoins")
				.then("he is not poor")
			}

			scenario("Given Then (no when, with blocks)") {
				given("Ben has some bitcoins") { _ in
					startingAmount
				}
				.then("this amount is greater than zero") { startingAmount in
					expect(startingAmount, isGreaterThan: 0)
				}
			}

			scenarioOutline("Scenario Outline with Examples") { example in
				given("A list of examples")
					.then("\(example) is one of them") { _ in
						expect(example, isGreaterThanOrEqual: 0)
					}
			}
			examples: {
				(0..<Int.random(in: 1...5)).map { $0 }
			}
		}
	}

	// MARK: - All Tests

	static var allTests = [
		("testGivenWhenThen", testGivenWhenThen)
	]
}
