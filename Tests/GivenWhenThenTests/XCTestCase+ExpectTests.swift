import XCTest
@testable import GivenWhenThen

final class XCTestCaseExpectTests: XCTestCase {

	// MARK: - Boolean

	/// Tests whether `XCTestCase.expect(isTrue:_:file:line:)` and `XCTestCase.expect(isFalse:_:file:line:)` work as expected.
	func testIsTrueFalse() throws {
		try feature("testIsTrueFalse") { [self] in
			scenarioOutline("expect(isTrue:) / expect(isFalse:)") { boolean in
				given("a boolean value that is \(boolean)", initialValue: boolean)
					.then("that value evaluates to \(boolean)") { boolean in
						if boolean {
							expect(isTrue: boolean)
						} else {
							expect(isFalse: boolean)
						}
					}
			}
			examples: {
				[true, false]
			}
		}
	}

	// MARK: - Optionals

	/// Tests whether `XCTestCase.expect(isNil:_:file:line:)` and `XCTestCase.expect(isNotNil:_:file:line:)` work as expected.
	func testIsNilNotNil() throws {
		try feature("testIsNilNotNil") { [self] in
			scenarioOutline("expect(isNil:) / expect(isNotNil:)") { optional in
				given("an optional value that is \(optional ?? "`nil`")", initialValue: optional)
					.then("that value evaluates to \(optional ?? "`nil`")") { optional in
						if optional == nil {
							expect(isNil: optional)
						} else {
							expect(isNotNil: optional)
						}
					}
			}
			examples: {
				[UUID().uuidString, nil]
			}
		}
	}

	// MARK: - Less Than

	/// Tests whether `XCTestCase.expect(_:isLessThan:_:file:line:)` works as expected.
	func testIsLessThan() throws {
		try feature("testIsLessThan") { [self] in
			scenario("expect(_:isLessThan:)") {
				given("two values", initialValue: ((Int.random(in: 1..<5), Int.random(in: 5..<10))))
					.when("one is less in value than the other")
					.then("expect(_:isLessThan:) succeeds if the first parameter is less than the second") { (firstValue, secondValue) in
						expect(firstValue, isLessThan: secondValue)
					}
			}
		}
	}

	/// Tests whether `XCTestCase.expect(_:isLessThanOrEqual:_:file:line:)` works as expected.
	func testIsLessThanOrEqual() throws {
		try feature("testIsLessThanOrEqual") { [self] in
			scenario("expect(_:isLessThanOrEqual:)") {
				given("three values", initialValue: (Int.random(in: 1..<5))) { firstValue in
					(firstValue, firstValue, Int.random(in: 5..<10))
				}
				.when("one is less in value and another is equal in value to the third")
				.then("expect(_:isLessThanOrEqual:) succeeds if the first parameter is less than the third") { (firstValue, secondValue, thirdValue) in
					forwarding((firstValue, secondValue)) {
						expect(firstValue, isLessThanOrEqual: thirdValue)
					}
				}
				.and("expect(_:isLessThanOrEqual:) succeeds if the first parameter is equal to the second") { (firstValue, secondValue) in
					expect(firstValue, isLessThanOrEqual: secondValue)
				}
			}
		}
	}

	// MARK: - Greater Than

	/// Tests whether `XCTestCase.expect(_:isGreaterThan:_:file:line:)` works as expected.
	func testIsGreaterThan() throws {
		try feature("testIsGreaterThan") { [self] in
			scenario("expect(_:isGreaterThan:)") {
				given("two values", initialValue: ((Int.random(in: 5..<10), Int.random(in: 1..<5))))
					.when("one is greater in value than the other")
					.then("expect(_:isGreaterThan:) succeeds if the first parameter is greater than the second") { (firstValue, secondValue) in
						expect(firstValue, isGreaterThan: secondValue)
					}
			}
		}
	}

	/// Tests whether `XCTestCase.expect(_:isGreaterThanOrEqual:_:file:line:)` works as expected.
	func testIsGreaterThanOrEqual() throws {
		try feature("testIsGreaterThanOrEqual") { [self] in
			scenario("expect(_:isGreaterThanOrEqual:)") {
				given("three values", initialValue: (Int.random(in: 1..<5))) { secondValue in
					(Int.random(in: 5..<10), secondValue, secondValue)
				}
				.when("one is greater in value and another is equal in value to the third")
				.then("expect(_:isGreaterThanOrEqual:) succeeds if the first parameter is greater than the third") { (firstValue, secondValue, thirdValue) in
					forwarding((firstValue, secondValue)) {
						expect(firstValue, isGreaterThanOrEqual: thirdValue)
					}
				}
				.and("expect(_:isGreaterThanOrEqual:) succeeds if the first parameter is equal to the second") { (firstValue, secondValue) in
					expect(firstValue, isGreaterThanOrEqual: secondValue)
				}
			}
		}
	}

	// MARK: - Equality

	/// Tests whether `XCTestCase.expect(_:isEqual:_:file:line:)` and
	/// `XCTestCase.expect(_:isNotEqual:_:file:line:)` work as expected.
	func testIsEqualIsNotEqual() throws {
		try feature("testIsGreaterThanOrEqual") { [self] in
			scenario("expect(_:isEqual:) / expect(_:isNotEqual:)") {
				given("three values", initialValue: (Int.random(in: 1..<5))) { equalValue in
					(equalValue, equalValue, Int.random(in: 5..<10))
				}
				.when("one is equal in value and another is not equal in value to the third")
				.then("expect(_:isNotEqual:) succeeds if the first parameter is not equal to the third") { (firstValue, secondValue, thirdValue) in
					forwarding((firstValue, secondValue)) {
						expect(firstValue, isNotEqual: thirdValue)
					}
				}
				.and("expect(_:isEqual:) succeeds if the first parameter is equal to the second") { (firstValue, secondValue) in
					expect(firstValue, isEqual: secondValue)
				}
			}
		}
	}

	// MARK: - All Tests

	static var allTests = [
		("testIsTrueFalse", testIsTrueFalse),
		("testIsNilNotNil", testIsNilNotNil),
		("testIsLessThan", testIsLessThan),
		("testIsLessThanOrEqual", testIsLessThanOrEqual),
		("testIsGreaterThan", testIsGreaterThan),
		("testIsGreaterThanOrEqual", testIsGreaterThanOrEqual),
		("testIsEqualIsNotEqual", testIsEqualIsNotEqual)
	]
}
